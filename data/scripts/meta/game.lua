require"scripts/multi_events"

local game_meta = sol.main.get_metatable"game"

local white_flash = require"scripts/fx/white_flash"

function game_meta:dx(offset)
  return {[0] = offset, [1] = 0, [2] = offset * -1, [3] = 0}
end

function game_meta:dy(offset)
  return {[0] = 0, [1] = offset * -1, [2] = 0, [3] = offset}
end

function game_meta:start_flash(color)
  if color then white_flash:set_color(color)
  else white_flash:set_color{255, 255, 255} end
  if sol.menu.is_started(white_flash) then
    sol.menu.stop(white_flash)
  end
  sol.menu.start(self, white_flash)
end

function game_meta:stop_flash()
  white_flash:fade_out(20, function() sol.menu.stop(white_flash) end)
end

return game_meta