local possible_items = {
  "solforge/basic_sword",
  "solforge/basic_spear",
  "solforge/basic_axe",
  "solforge/basic_fire_sword",
}
--could call this instead: local possible_items = sol.main.get_items_in_directory("items/solforge")
--but then you couldn't order them non-alphabetically

local menu = require("scripts/menus/inventory/bottomless_list"):build{
  all_items = possible_items,
  num_columns = 4,
  num_rows = 5,
  menu_x = 16
}

function menu:init(game)
  if sol.main.debug_mode then
    for _, v in ipairs(possible_items) do game:get_item(v):set_variant(1) end
  end
end

menu:register_event("on_command_pressed", function(self, cmd)
  local game = sol.main.get_game()
  if cmd == "action" then
    local item = menu:get_current_item()
    game:set_value("equipped_weapon", item:get_name())
    sol.audio.play_sound"cursor"

  elseif cmd == "attack" then

  end
end)

menu:register_event("on_started", function()
  sol.main.get_game():set_suspended(true)
end)

menu:register_event("on_finished", function()
  sol.main.get_game():set_suspended(false)
end)


return menu