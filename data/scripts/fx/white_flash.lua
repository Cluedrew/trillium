local menu = {}

local white_surface = sol.surface.create()
white_surface:fill_color{255,255,255}

function menu:on_started()
  white_surface:set_opacity(0)
  white_surface:fade_in(2)
end

function menu:fade_out(delay, callback)
  if type(delay) == "function" then
    callback = delay
    delay = 20
  end
  if not callback then
    callback = function() print"CALLING BACK" sol.menu.stop(menu) end
  end
  white_surface:fade_out(delay, callback)
end

function menu:on_draw(dst)
  white_surface:draw(dst)
end

function menu:set_color(color)
  white_surface:fill_color(color)
end

return menu