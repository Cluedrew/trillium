local manager = {}

function manager:apply_behavior(enemy)
  local game = enemy:get_game()
  local map = enemy:get_map()
  local hero = map:get_hero()
  local sprite = enemy:get_sprite()

  function enemy:ram_attack(props)
    props = props or {}
    local damage = props.damage or enemy:get_damage() or 2
    local type = props.type or nil
    local orthogonal_ram = props.orthogonal_ram or false
    local angle_locked_at_windup = props.angle_locked_at_windup or false
    local speed = props.speed or 180
    local smooth = props.smooth or false
    local max_distance = props.max_distance or 100
    local windup_animation = props.windup_animation or "stopped"
    local windup_duration = props.windup_duration or 500
    local attack_animation = props.attack_animation or "walking"
    local weapon_sprite = props.weapon_sprite
    local weapon_windup_animation = props.weapon_windup_animation
    local weapon_attack_animation = props.weapon_attack_animation

    local sprite = enemy:get_sprite()
    local angle
    if angle_locked_at_windup then
      angle = orthogonal_ram and enemy:get_direction4_to(hero) * math.pi / 2 or enemy:get_angle(hero)
    end
    enemy:stop_movement()
    sprite:set_animation(windup_animation)
    local x, y, z = enemy:get_position()
    local weapon_entity = map:create_custom_entity{
      x=x, y=y, layer=z, direction = sprite:get_direction(), width = 16, height = 16,
      sprite = weapon_sprite,
    }
    weapon_entity:add_collision_test("sprite", function(weapon_entity, other)
      if other:get_type() == "hero" and not hero:is_invincible() then
        weapon_entity:clear_collision_tests()
        if hero.process_hit then
          hero:process_hit({damage = damage, enemy = enemy, type = type})
        else
          hero:start_hurt(enemy, damage)
        end
      end
    end)
    weapon_entity:get_sprite():set_animation(weapon_windup_animation)
    function weapon_entity:on_update()
      weapon_entity:set_position(enemy:get_position())
      weapon_entity:get_sprite():set_direction(sprite:get_direction())
      if enemy:get_life() <= 0 then weapon_entity:remove() end
    end
    sol.timer.start(enemy, windup_duration, function()
      if not angle_locked_at_windup then
        angle = orthogonal_ram and enemy:get_direction4_to(hero) * math.pi / 2 or enemy:get_angle(hero)
      end
      sprite:set_animation(attack_animation)
      weapon_entity:get_sprite():set_animation(weapon_attack_animation)
      local m = sol.movement.create"straight"
      m:set_angle(angle)
      m:set_speed(speed)
      m:set_smooth(smooth)
      m:set_max_distance(max_distance)
      m:start(enemy, function() enemy:finish_ram() end)
      function m:on_obstacle_reached() enemy:finish_ram() end

      function enemy:finish_ram() 
        sprite:set_animation"stopped"
        weapon_entity:remove()
        enemy:decide_action()
      end
    end)
  end

end

return manager
