--An entity that damages enemies.
--[[
optional parameters to set are:
entity.damage (how much damage it causes, default 1)
entity.damage_cooldown (cooldown between hurting the same entity another time, default 200ms)
entity.duration (how long the entity lasts)
--]]

local entity = ...
local game = entity:get_game()
local map = entity:get_map()

function entity:on_created()
  entity.collided_entities = {}
  entity:add_collision_test("sprite", function(entity, other)
    if other:get_type() == "enemy" and not entity.collided_entities[other] then
      entity.collided_entities[other] = other
      sol.timer.start(entity, entity.damage_cooldown or 200, function() entity.collided_entities[other] = nil end)
      other:hurt(entity.damage or 1)
    end
  end)

  --determine removal
  sol.timer.start(entity, 10, function()
    if entity.duration then
      sol.timer.start(entity, entity.duration, function() entity:remove() end)
    else
      local sprite = entity:get_sprite()
      sprite:set_animation(sprite:get_animation(), function() entity:remove() end)
    end
  end)
end


