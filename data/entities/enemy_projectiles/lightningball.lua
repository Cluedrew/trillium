local entity = ...
local game = entity:get_game()
local map = entity:get_map()
local hero = map:get_hero()
local movement


function entity:on_created()
  entity.is_projectile = true
  entity:set_drawn_in_y_order(true)
  entity:set_can_traverse("enemy", true)
  entity:set_can_traverse_ground("deep_water", true)
  entity:set_can_traverse_ground("hole", true)
  entity:set_can_traverse_ground("lava", true)
  entity:set_can_traverse_ground("low_wall", true)
end

function entity:shoot(angle)
  entity:get_sprite():set_rotation(angle)
  local type = entity.damage_type or "physical"

  local m = sol.movement.create"straight"
  m:set_speed(250)
  m:set_angle(angle)
  m:set_max_distance(700)
  m:set_smooth(false)
  m:start(entity, function() entity:hit_obstacle() end)
  function m:on_obstacle_reached()
    entity:hit_obstacle()
  end
  function m:on_changed()
    entity:hit_obstacle()
  end

  entity:add_collision_test("sprite", function(entity, other)
    if other:get_type() == "hero" and not hero:is_invincible() then
      entity:clear_collision_tests()
      local damage = entity.damage or 1
      if hero.process_hit then
        hero:process_hit({damage = damage, enemy = entity, type = type})
      else
        hero:start_hurt(entity, damage)
      end
      entity:hit_obstacle()
    end
  end)

end


function entity:hit_obstacle()
  entity:stop_movement()
  local x,y,z = entity:get_position()
  local blast = map:create_lightning({
    x=x, y=y, layer=z, type="lightning_zap"
  })
  local pop_sprite = entity:create_sprite("enemies/enemy_killed_projectile")
  entity:remove_sprite()
  pop_sprite:set_animation("killed", function() entity:remove() end)
end

