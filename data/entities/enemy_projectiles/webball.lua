local entity = ...
local game = entity:get_game()
local map = entity:get_map()
local hero = map:get_hero()
local sprite
local movement


function entity:on_created()
  entity.is_projectile = true
  entity:set_drawn_in_y_order(true)
  entity:set_can_traverse("enemy", true)
  entity:set_can_traverse_ground("deep_water", true)
  entity:set_can_traverse_ground("hole", true)
  entity:set_can_traverse_ground("lava", true)
  entity:set_can_traverse_ground("low_wall", true)
end

function entity:shoot(angle)
  local damage = entity.damage or 1
  local type = entity.damage_type or "physical"
  local max_distance = entity.max_distance or 700
  local speed = entity.speed or 200

  entity:get_sprite():set_rotation(angle)

  local m = sol.movement.create"straight"
  m:set_speed(speed)
  m:set_angle(angle)
  m:set_max_distance(max_distance)
  m:set_smooth(false)
  m:start(entity, function() entity:hit_obstacle() end)
  function m:on_obstacle_reached()
    entity:hit_obstacle()
  end
  function m:on_changed()
    entity:hit_obstacle()
  end

  entity:add_collision_test("sprite", function(entity, other)
    if other:get_type() == "hero" and not hero:is_invincible() then
      entity:clear_collision_tests()
      if hero.process_hit then
        hero:process_hit({damage = damage, enemy = entity, type = type})
      else
        hero:start_hurt(entity, damage)
      end
      game:start_status_effect("webbed", 3000)
      entity:hit_obstacle()
    end
  end)

end


function entity:hit_obstacle()
  entity:stop_movement()
  local pop_sprite = entity:create_sprite("enemies/enemy_killed_projectile")
  entity:remove_sprite()
  pop_sprite:set_animation("killed", function() entity:remove() end)
end

