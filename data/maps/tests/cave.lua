local map = ...
local game = map:get_game()

map:register_event("on_started", function()
  map:set_darkness_level({30,35,50})
end)