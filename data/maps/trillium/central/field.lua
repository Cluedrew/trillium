local map = ...
local game = map:get_game()

map:register_event("on_started", function()
  for e in map:get_entities("grid_ref") do
    e:remove()
  end
end)
